import express from 'express'
import musicRouter from './musicRouter.js'
import { initiateCreateArtistsTable, initiateCreateAlbumsTable } from '../db/db.js'

initiateCreateArtistsTable()
initiateCreateAlbumsTable()

const server = express()
server.use(express.json())

server.use(musicRouter)

server.get('/', async (_req, res) => {
    res.send(`
    <h1>Music DB</h1>

    <h4>GET</h4>
    <p>/artists to get artists</p>
    <p>/albums to get albums</p>

    <p>/artists/:id to get specific artist</p>
    <p>/albums/:id to get specific album</p>

    <h4>DELETE</h4>

    <p>/artists/:id to delete specific artist</p>
    <p>/albums/:id to delete specific album</p>

    <h4>POST</h4>

    <p>/artists to insert an artist</p>
    <p>request body must include {
        name: string
    }</p>

    <p>/albums to insert an album</p>
    <p>request body must include {
        name: string,
        artist_id: int,
        release_year: int
    }</p>

    <h4>PUT</h4>

    <p>/artists/:id to update specific artist</p>
    <p>request body must include {
        name: string
    }</p>

    <p>/albums/:id to update specific album</p>
    <p>request body must include {
        name: string,
        artist_id: int,
        release_year: int
    }</p>
    `)
})


export default server
