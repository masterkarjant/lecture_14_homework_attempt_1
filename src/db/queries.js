

export const showAllArtists = 'SELECT * FROM artists;'
export const showAllAlbums = 'SELECT * FROM albums;'

export const showTargetArtist = 'SELECT * FROM artists WHERE id = $1;'
export const showTargetAlbum = 'SELECT * FROM albums WHERE id = $1;'

export const insertTargetArtist = 'INSERT INTO artists (name) VALUES ($1);'
export const insertTargetAlbum = 'INSERT INTO albums (name, artist, release_year) VALUES ($1, $2, $3);'

export const updateTargetArtist = 'UPDATE artists SET name = $2 WHERE id = $1;'
export const updateTargetAlbum = 'UPDATE albums SET name = $2, artist = $3, release_year = $4 WHERE id = $1'

export const deleteTargetArtist = 'DELETE FROM artists WHERE id = $1;'
export const deleteTargetAlbum = 'DELETE FROM albums WHERE id = $1'

export const createArtistsTable = `
CREATE TABLE IF NOT EXISTS "artists" (
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL
);`

export const createAlbumsTable = `
CREATE TABLE IF NOT EXISTS "artists" (
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(100) NOT NULL,
    "artist" INTEGER,
    "release_year" SMALLINT,
    CONSTRAINT fk_artist FOREIGN KEY(artist) REFERENCES artists(id) ON DELETE SET NULL
);`