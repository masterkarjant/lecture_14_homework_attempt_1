import * as queries from './queries.js'
import executeQuery from './db.js'
// - GET ALL

const getAllArtists = async () => {
    console.log('Executing query "showAllArtists"')
    const result = await executeQuery(queries.showAllArtists)
    return result.rows
}

const getAllAlbums = async () => {
    console.log('Executing query "showAllAlbums"')
    const result = await executeQuery(queries.showAllAlbums)
    return result.rows
}

// - GET SINGLE

const getTargetArtist = async (id) => {
    console.log('Executing query "showTargetArtist"', id)
    const result = await executeQuery(queries.showTargetArtist, [id])
    return result.rows[0]
}

const getTargetAlbum = async (id) => {
    console.log('Executing query "showTargetAlbum"', id)
    const result = await executeQuery(queries.showTargetAlbum, [id])
    return result.rows[0]
}

// - INSERT 

const insertTargetArtist = async (body) => {
    console.log('Executing query "insertTargetArtist"', body)
    const result = await executeQuery(queries.insertTargetArtist, [body.name])
    return result.rows[0]
}

const insertTargetAlbum = async (body) => {
    console.log('Executing query "insertTargetAlbum"', body)
    const result = await executeQuery(queries.insertTargetAlbum, [body.name, body.artist_id, body.release_year])
    return result.rows[0]
}

// - UPDATE 

const updateTargetArtist = async (id, body) => {
    console.log('Executing query "updateTargetArtist"', body)
    const result = await executeQuery(queries.updateTargetArtist, [id, body.name])
    return result.rows[0]
}

const updateTargetAlbum = async (id, body) => {
    console.log('Executing query "updateTargetAlbum"', body)
    const result = await executeQuery(queries.updateTargetAlbum, [id, body.name, body.artist_id, body.release_year])
    return result.rows[0]
}

// - DELETE 

const deleteTargetArtist = async (id) => {
    console.log('Executing query "deleteTargetArtist"')
    const result = await executeQuery(queries.deleteTargetArtist, [id])
    return result.rows[0]
}

const deleteTargetAlbum = async (id) => {
    console.log('Executing query "deleteTargetAlbum"')
    const result = await executeQuery(queries.deleteTargetAlbum, [id])
    return result.rows[0]
}

export { getAllArtists, getAllAlbums, getTargetArtist, getTargetAlbum, insertTargetArtist, insertTargetAlbum, updateTargetArtist, updateTargetAlbum, deleteTargetArtist, deleteTargetAlbum } 