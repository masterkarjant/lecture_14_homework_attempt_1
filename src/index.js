import server from './api/server.js'

const PORT = process.env.PORT || 3001
const { PG_HOST, PG_USERNAME, PG_DATABASE, PG_PASSWORD, PG_PORT } = process.env

server.listen(PORT, () => {
    console.log('Version 1')
    console.log('Environment variables:', { PG_HOST, PG_USERNAME, PG_DATABASE, PG_PASSWORD, PG_PORT, PORT })
    console.log(`Listening to ${PORT}`)
})

// PORT=3232
// PG_HOST=localhost
// PG_PORT=5432
// PG_USERNAME=pguser
// PG_PASSWORD=pgpass
// PG_DATABASE=musicdb

