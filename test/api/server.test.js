/* eslint-disable no-undef */
import request from 'supertest'
import server from '../../src/api/server.js'

describe('Basic Tests', () => {

    it('Testing True = True', () => {
        expect(true).toBe(true)
    })

    it('Testing Server For 200', async () => {
        const response = await request(server).get('/')
        expect(response.status).toBe(200)
    })
})